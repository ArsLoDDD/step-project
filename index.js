const header = $('header')
const serviceItems = $('.service-list li')
const serviceList = $('.service-list')
const servicePhoto = $('.service-info-block-photo')
const serviceParagraph = $('.service-info-block p')
const loremArray = [
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
	'Nulla sed justo vel odio ullamcorper posuere a et tellus.',
	'Donec sit amet felis eu sapien interdum mattis.',
	'Nam sed massa nec ligula fringilla faucibus vel a nisi.',
	'Morbi maximus tellus eu turpis finibus, quis malesuada arcu feugiat.',
	'Sed in bibendum velit, eu elementum justo.',
	'Quisque vel turpis sit amet massa rutrum bibendum.',
	'Praesent in nunc enim.',
	'Suspendisse id ultrices leo.',
	'Fusce volutpat, tortor eu mattis tristique, nisl velit bibendum sapien, ac eleifend tellus augue a ante.',
	'Aliquam bibendum malesuada nisi non tempor.',
	'Suspendisse potenti.',
	'Etiam ut velit quam.',
	'Cras ac libero vel enim fringilla mollis.',
	'Phasellus et consequat augue, ac rutrum leo.',
	'Mauris pharetra nisl id nulla vulputate congue.',
	'Proin nec ex quis libero scelerisque interdum.',
	'Aenean ac posuere ex.',
	'In sit amet magna volutpat, malesuada dolor ut, malesuada ex.',
]
const amazingArray = [
	'top performer',
	'visual creativity',
	'CREATIVE DESIGN',
	'online presence',
	'website building',
]
const logoContainer = $('.logo-container')
const logoLetter = $('.logo span')
const searchIcon = $('.svg-search')
const searchInput = $('.input-box input')
const serviceInfoBlock = $('.service-info-block')
const line = $('.line.frst-line')
const scdLine = $('.scd-line')
const amazingWorkItems = $('.amazing-work-item')
const amazingWorkList = $('.amazing-work-list li')
const amazingWorkItemBox = $('.amazing-work-item-box')
const loadMoreBtn = $('.load-more')
const spiner = $('.lds-spinner')
const svgLoadMore = $('svg-load')
const amazingHover = $('.amazing-hover')

let isFrstScroll = false
let isScdScroll = false

scrollLine = lineIndex => {
	$(line[lineIndex]).css({
		transform: 'translateX(0px)',
		transition: 'all .6s ease-in-out',
	})
	$(scdLine[lineIndex]).css({
		left: '48%',
		transition: 'all .6s ease-in-out',
	})
	isFrstScroll = true
}

$(window).scroll(() => {
	console.log($(window).scrollTop())
	if ($(window).scrollTop() > 200) {
		$(header).css({
			boxShadow: '0 7px 5px rgba(57, 63, 72, 0.3)',
		})
	} else if ($(window).scrollTop() < 200) {
		$(header).css({
			boxShadow: 'none',
		})
	}

	if ($(window).scrollTop() > 725) {
		scrollLine(0)
	}
	if ($(window).scrollTop() > 1750) {
		scrollLine(1)
	}
	if ($(window).scrollTop() > 3025) {
		scrollLine(2)
	}
	if ($(window).scrollTop() > 3795) {
		scrollLine(3)
	}
	if ($(window).scrollTop() > 4865) {
		scrollLine(4)
	}
	if ($(window).scrollTop() < 690 && isFrstScroll) {
		resetLinePosition(0)
	}
	if ($(window).scrollTop() < 1725 && isFrstScroll) {
		resetLinePosition(1)
	}
	if ($(window).scrollTop() < 3000 && isFrstScroll) {
		resetLinePosition(2)
	}
	if ($(window).scrollTop() < 3770 && isFrstScroll) {
		resetLinePosition(3)
	}
	if ($(window).scrollTop() < 4840 && isFrstScroll) {
		resetLinePosition(4)
	}
})

resetLinePosition = index => {
	$(line[index]).css({
		transform: 'translateX(700px)',
	})
	$(scdLine[index]).css({
		left: '1%',
	})
}

resetLinePosition()

randomText = () => {
	let loremText = ''
	for (let i = 0; i < 13; i++) {
		const randomIndex = Math.floor(Math.random() * loremArray.length)
		loremText += loremArray[randomIndex] + ' '
	}
	$(serviceParagraph).html(`<p>${loremText}</p>`)
}

indexPhoto = (index = 0) => {
	$(servicePhoto).css({
		height: $(serviceParagraph).height() + 'px',
		backgroundImage: `url(./image/main-scd-block/service-${index}.jpeg)`,
	})
}

randomText()

indexPhoto()

$(serviceItems).click(function () {
	$(this).siblings().removeClass('active-service')
	$(this).addClass('active-service')
	$(this).css({
		transition: 'transform 0.3s ease-in-out',
		transform: 'translateY(-25px)',
	})
	$(serviceInfoBlock).css({
		transition: 'all 0.3s ease-in-out',
		transform: 'translateY(30px)',
		opacity: 0,
	})

	setTimeout(() => {
		randomText()
		indexPhoto($(this).index())
	}, 300)

	setTimeout(() => {
		$(this).css({
			transform: 'translateY(0px)',
		})
	}, 330)

	setTimeout(() => {
		$(serviceInfoBlock).css({
			transform: 'translateY(0px)',
			opacity: 1,
		})
	}, 380)
})

$(logoContainer).mouseenter(function () {
	$(logoLetter).css({
		transition: 'transform 0.3s ease-in-out',
		transform: 'rotateZ(180deg)',
	})
})
$(logoContainer).mouseleave(function () {
	$(logoLetter).css({
		transition: 'transform 0.3s ease-in-out',
		transform: 'rotateZ(-180deg)',
	})
})

let searchIconClick = false
let searchIconDeg = null

searchIcon.click(function () {
	if (searchIconClick === false) {
		searchIconDeg = '90deg'
		searchIconClick = true
	} else {
		searchIconDeg = '0deg'
		searchIconClick = false
	}
	$(this).css({
		transition: 'transform 0.3s ease-in-out',
		transform: `rotateZ(${searchIconDeg})`,
	})
	$(searchInput).css({
		opacity: `${searchIconClick ? 1 : 0}`,
		pointerEvents: `${searchIconClick ? 'auto' : 'none'}`,
	})
})
let firstLoad = true
pullItemCard = array => {
	if (firstLoad) {
		$(array).each(function (index) {
			$(this).css({
				backgroundImage: `url(./image/amazing/amazing-work-item-${$(this).data(
					'index'
				)}.png)`,
			})
		})
		firstLoad = false
	} else {
		clearTable()
	}
}
let filterIndex = 0
let newLoad = false
let currentArray = null
let rewriteArray = false
filterItemCard = (array, wotkIndex = filterIndex) => {
	console.log(array)
	const newItemCardList = $(array).filter(function (index) {
		if (wotkIndex === $(array[index]).data('work')) {
			return $(array[index])
		}
	})
	console.log(newItemCardList)
	if (newLoad) {
		return newItemCardList
	}
	$('.amazing-work-item-box').html(oldArray)

	clearTable()
	$('.amazing-work-item-box').html(newItemCardList)
	currentArray = newItemCardList
	console.log(currentArray)
}

clearTable = () => {
	$('.amazing-work-item-box').each(function (index) {
		$(this).html('')
	})
}
let itemLength = amazingWorkItems.length
addLoadMoreItems = () => {
	const newAmazingWorkItems = []

	for (let i = itemLength; i < itemLength + 12; i++) {
		let el = `<div style="background-image: url(https://picsum.photos/id/${i}/212/285
		);" data-index="${i}" data-new='true'  data-work='${
			Math.floor(Math.random() * 4) + 1
		}'   class="amazing-work-item"><div class="  amazing-hover inner-amazing-item">
		<div class="creative-design-icon">
			<div class="chain-icon">
				<svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" clip-rule="evenodd"
						d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
						fill="#1FDAB5" />
				</svg>

			</div>
			<div class="square-icon">
				<div class="square-icon-inner"></div>
			</div>
		</div>
		<div class="creative-design-text">
			<span class="uppercase">top performer</span>
			<span>All</span>
		</div>
	</div>
		</div>`
		$(el).css({
			backgroundColor: 'red',
		})
		newAmazingWorkItems.push(el)
	}
	newLoad = true
	clearTable()
	$(amazingWorkItemBox).append(newAmazingWorkItems)
	console.log(currentArray)

	const newnewnew = filterItemCard(amazingWorkItemBox.children())
	console.log(currentArray)

	clearTable()
	if (filterIndex !== 0) {
		$(amazingWorkItemBox).append($.merge(currentArray, newnewnew.prevObject))
	} else {
		$(amazingWorkItemBox).append($.merge(oldArray, newnewnew.prevObject))
	}

	itemLength = itemLength * 2
	newLoad = false
}
let maxLoadMore = 0
$('.load-more-box')
	.eq(0)
	.click(function () {
		if (maxLoadMore === 2) {
			$('.load-more-box').fadeOut(300)
			return
		}
		maxLoadMore++

		$('.lds-roller').toggleClass('disabled')
		$('.svg-load-box').toggleClass('disabled')
		$('.load-more span').html('Loading...')
		$('.load-more-box').eq(0).css({
			pointerEvents: 'none',
		})
		setTimeout(() => {
			addLoadMoreItems()
		}, 2000)
		setTimeout(() => {
			$('.lds-roller').toggleClass('disabled')
			$('.svg-load-box').toggleClass('disabled')
			$('.load-more-box').eq(0).css({
				pointerEvents: 'auto',
			})
			$('.load-more span').html('Load More')
		}, 2000)
		if (maxLoadMore === 2) {
			$('.load-more-box').eq(0).fadeOut(2500)
		}
	})

pullItemCard(amazingWorkItems)
amazingHover.mouseenter(function () {
	// amazingArray[$(this).parent().data('work')]
	$(this).children().children()[2].textContent =
		amazingArray[$(this).parent().data('work')]
	$(this).children().children()[3].textContent =
		$('.service-name')[$(this).parent().data('work')].textContent
})
changeStyleBtn = (index, bool) => {
	if (bool === true) {
		$(amazingWorkList[index]).css({
			color: '#18cfab',
			border: '1px solid #18cfab',
		})
	} else {
		$(amazingWorkList[index]).css({
			color: 'black',
			border: 'none',
		})
	}
}
let lastActiveBtn = 0

changeStyleBtn(lastActiveBtn, true)
amazingWorkList.click(function () {
	changeStyleBtn(lastActiveBtn, false)
	lastActiveBtn = $(this).index()
	changeStyleBtn(lastActiveBtn, true)
	$('.creative-design-text span:last-of-type').html($(this).children().html())
	$('.creative-design-text span:first-of-type').html(
		amazingArray[$(this).index()]
	)
	if ($(this).index() === 0) {
		$('.amazing-work-item-box').html(oldArray)
		filterIndex = 0
		return
	}
	filterIndex = $(this).index()
	filterItemCard(oldArray, $(this).index())
})

let oldArray = $('.amazing-work-item-box').children()

const monthArray = [
	'JAN',
	'FEB',
	'MAR',
	'APR',
	'MAY',
	'JUN',
	'JUL',
	'AUG',
	'SEP',
	'OCT',
	'NOV',
	'DEC',
]

const authorArray = ['admin', 'moder', 'user']
loadNewsItem = () => {
	const newsListArray = []
	for (let i = 1; i < 8; i++) {
		const el = `<li class="news-item">
<a href="">
	<img src="./image/news/news-${i}.png" alt="">
	<div class="news-item-text"><span class="news-item-day">${
		Math.floor(Math.random() * 28) + 1
	}</span><span
			class="news-item-month">${monthArray[Math.floor(Math.random() * 11)]}</span>
	</div>
</a>
<div class="new-name-box">
	<a href="" class="news-name">Amazing Blog Post</a>
	<div class="news-name-info"><span class="author-name">By ${
		authorArray[Math.floor(Math.random() * 3)]
	}</span><span class="news-count-comments">${Math.floor(Math.random() * 9) + 1}
			comment</span>
	</div>
</div>
</li>`
		newsListArray.push(el)
	}
	$('.news-item-list').append(newsListArray)
}
loadNewsItem()
randomSay = () => {
	let loremText = ''
	for (let i = 0; i < 5; i++) {
		const randomIndex = Math.floor(Math.random() * loremArray.length)
		loremText += loremArray[randomIndex] + ' '
	}
	return loremText
}
let workerIndex = 0
let workerSecFrstLoad = true
let workerNames = [
	'Olivia Smith',
	'Stan Williams',
	'Charlotte Johnson',
	'James Brown',
]
let workerPosition = [
	'UX Designer',
	'Web Developer',
	'UI Designer',
	'Web Designer',
]
loadWorkerPhoto = () => {
	$('.mini-photo-box')
		.children()
		.each(function () {
			$(this).removeClass('active')
		})
	const workerPhoto = `<img class="people-main-photo" src="./image/people/people-${workerIndex}.jpg" alt="">
</div>`
	$('.people-text').html(randomSay())
	$('.people-info-name').html(workerNames[workerIndex])
	$('.people-info-job').html(workerPosition[workerIndex])
	$('.people-info-photo-box').html('')
	$('.people-info-photo-box').append(workerPhoto)
	$('.mini-photo-box').children().eq(workerIndex).addClass('active')
}
loadMiniPhoto = () => {
	const miniPhotoArray = []
	for (let i = 0; i < 4; i++) {
		const el = `<div class="mini-photo">
<img src="./image/people/people-${i}.jpg" alt="">
</div>`
		miniPhotoArray.push(el)
	}
	$('.mini-photo-box').append(miniPhotoArray)
}
loadMiniPhoto()
if (workerSecFrstLoad === true) {
	loadWorkerPhoto()
	workerSecFrstLoad = false
}
// setInterval(() => {
// 	loadWorkerPhoto()
// }, 6000)

$('.right-arrow').click(() => {
	workerIndex + 1 > $('.mini-photo-box').children().length - 1
		? (workerIndex = 0)
		: (workerIndex = workerIndex + 1)
	loadWorkerPhoto()
})
$('.left-arrow').click(() => {
	console.log(workerIndex)

	workerIndex - 1 < 0
		? (workerIndex = $('.mini-photo-box').children().length - 1)
		: (workerIndex = workerIndex - 1)
	console.log(workerIndex)
	loadWorkerPhoto()
})
loadGalleryItems = () => {
	const galleryArray = []
	for (let i = 0; i < 18; i++) {
		const el = `<div class="image-gallery image-gallery-${i}"></div>`
		galleryArray.push(el)
	}
	$('.gallery-grid-box').append(galleryArray)
	$('.gallery-grid-box')
		.children()
		.each(function (index) {
			$(this).css({
				background: `url(https://picsum.photos/seed/${
					Math.floor(Math.random() * 300) + 1
				}/800/600)`,
				backgroundSize: 'cover',
				backgroundPosition: 'center',
			})

			let galleryHover = `<div class="gallery-hover">
			<div class="gallery-hover-icon-frst"></div>
			<div class="gallery-hover-icon-scd"></div></div>`
			$(this).append(galleryHover)
		})
}
loadGalleryItems()
addLoadMoreItemsBlock = () => {
	console.log(1)
	const galleryArray = []
	for (
		let i = $('.gallery-grid-box').children().length;
		i < $('.gallery-grid-box').children().length + 6;
		i++
	) {
		const el = `<div class="image-gallery-${i}"></div>`
		galleryArray.push(el)
	}
	$('.gallery-grid-box').append(galleryArray)
	for (let index = 18; index < 24; index++) {
		const element = $('.gallery-grid-box').children()[index]

		$(element).css({
			background: `url(https://picsum.photos/seed/${
				Math.floor(Math.random() * 300) + 1
			}/800/600)`,
			backgroundSize: 'cover',
			backgroundPosition: 'center',
		})
	}
	$('.gallery-grid-box').css({
		height: '1290px',
		marginBottom: '145px',
	})
	$('.gallery-grid-box').css({
		gridTemplateRows: 'repeat(29, 1fr)',
	})
	$('.image-gallery-18').css({
		gridArea: '16 / 13 / 22 / 19',
	})
	$('.image-gallery-19').css({
		gridArea: '16 / 1 / 22 / 7',
	})
	$('.image-gallery-20').css({
		gridArea: '17/ 7 / 22 / 13',
	})
	$('.image-gallery-21').css({
		gridArea: '22 / 1 / 30 / 7',
	})
	$('.image-gallery-22').css({
		gridArea: '22 / 7 / 30 / 13',
	})
	$('.image-gallery-23').css({
		gridArea: '22 / 13 / 30 / 19',
	})
	setTimeout(() => {
		$('.gallery-grid-box').css({
			marginBottom: '30px',
		})
	}, 500)
}
$('.load-more-box')
	.eq(1)
	.click(function () {
		$('.lds-roller').toggleClass('disabled')
		$('.svg-load-box').toggleClass('disabled')
		$('.load-more span').html('Loading...')
		$('.load-more-box').eq(1).css({
			pointerEvents: 'none',
		})
		setTimeout(() => {
			addLoadMoreItemsBlock()
		}, 2000)
		setTimeout(() => {
			$('.lds-roller').toggleClass('disabled')
			$('.svg-load-box').toggleClass('disabled')
			$('.load-more-box').eq(1).css({
				pointerEvents: 'auto',
			})
			$('.load-more span').html('Load More')
		}, 2000)

		$('.load-more-box').eq(1).fadeOut(2500)
	})
$('.mini-photo').click(function () {
	workerIndex = $(this).index()
	loadWorkerPhoto()
})
